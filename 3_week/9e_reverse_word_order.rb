# Given a string, write a program to reverse the word order (rather than character order)
#
# doctest: I pass in string and get back the same in reverse order
# >> reverse_string "This is ruby learning course"
# => "course learning ruby is This"

def reverse_string str
  str.split(" ").reverse.join(" ")
end
