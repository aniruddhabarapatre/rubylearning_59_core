# Write below code in 1 line (DRY: ruby way)

# The long way
record = Hash.new
record[:name] = "Satish"
record[:email] = "mail@satishtalim.com"
record[:phone] = "919371006659"

record1 = {name: "Satish", email: "mail@satishtalim.com", phone: "919371006659"}

puts record == record1
