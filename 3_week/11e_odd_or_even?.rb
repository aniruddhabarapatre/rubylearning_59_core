# Write a Ruby program that, when given an array:
#
# collection = [12, 23, 456, 123, 4579]
# prints each number, and tells you whether it is odd or even.

def odd_even array
  array.each do |num|
    puts num.even? ? "#{num}: even" : "#{num}: odd"
  end
end

if __FILE__ == $PROGRAM_NAME
  collection = [12, 23, 456, 123, 4579]
  odd_even collection
end
