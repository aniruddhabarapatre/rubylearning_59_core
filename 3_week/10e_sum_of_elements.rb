# Write a Ruby program that, when given an array:
#
# collection = [1, 2, 3, 4, 5]
# calculates the sum of its elements.
#
# doctest: I pass in array and get sum of elements
# >> sum_array_elements [1, 2, 3]
# => 6

def sum_array_elements array
  sum = 0
  array.each do |num|
    sum = sum + num
  end
  sum
end

if __FILE__ == $PROGRAM_NAME
  collection = [1, 2, 3, 4, 5]
  sum_array_elements collection
end
