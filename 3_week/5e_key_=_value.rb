# Given a string s = 'key=value', create two strings s1 and s2 such that s1 contains
# key and s2 contains value. Hint: Use some of the String functions.

# s1, s2 = s.split("=")

# doctest: I pass in string and get back key value separated
# >> separate_key_value 'key=value'
# => ["key", "value"]

def separate_key_value str
  s1, s2 = str.split("=")
end

if __FILE__ == $PROGRAM_NAME
  s = 'key=value'
  puts separate_key_value s
end
