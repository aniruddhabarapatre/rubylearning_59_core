=begin
doctest: I can greet the world by calling hello
>> hello
=> "Hello World!"
doctest: I can greet "Bob" by caling hello "Bob"
>> hello "Bob"
=> "Hello Bob!"
doctest: I can ask if someone is there
>> hello "Santa", "?"
=> "Hello Santa?"
=end

def hello name = 'World', punctuation = '!'
 "Hello #{name}#{punctuation}"
end


