# Write a program to display how old I am?Display result in floting point
# number upto 2 decimal.

# doctest: I pass in number of seconds to get age in years
# >> format("%0.2f", age_in_years(979000000))
# => "31.04"
#
# doctest: age_in_years returns a numeric
# >> age_in_years(0).is_a? Numeric
# => true
#
# doctest: given a 0 should be 0
# >> age_in_years(0)
# => 0
#
# doctest: What if I give it a string that doesn't look like a number?
# This is tricky, because we need to know how to rescue an error.
# >> ->(seconds) {begin ; age_in_years(seconds) ; rescue ArgumentError => e ; e.class ; end }.call('Look Ma! No Hands!')
# => ArgumentError
#
# doctest: If I give it a string that looks like a number... "15" should not be 42
# Just making sure it is 'correct enough' by saying if it is good to 6
# decimals, perhap it is good enough
# >> age_in_years("9.79e8").round(6)
# => 31.043886

def age_in_years seconds
  Float(seconds) / (60 * 60 * 24 * 365)
end

if __FILE__ == $0
  # Going to write a program down here to figure out the syntax in a line for the test above.
  begin
    age_in_years("look ma")
  rescue ArgumentError => e
    puts e.class
  end
end
