# Write a Ruby program that tells you how many minutes are there in a year
#
# doctest: I can get minutes in a year
# >> minutes_in_year
# => 525600
#
def minutes_in_year
  60 * 24 * 365
end
