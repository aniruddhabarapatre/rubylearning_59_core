# Write a method called convert which takes temperature in Fahrenheit and
# returns temprature in celcius.

# doctest: I pass temperature in deg F to get it back in deg C
# >> format("%.2f",convert(90))
# => "32.22"

def _Fahrenheit_to_Celcius temp
  (temp - 32.0) * 5 / 9
end

alias :convert :_Fahrenheit_to_Celcius
