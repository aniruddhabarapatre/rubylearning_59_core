# Write a method leap_year?. It should accept a year value from the user, check whether
# it's a leap year, and then return true or false. With the help of this leap_year?()
# method calculate and display the number of minutes in a leap year (2000 and 2004) and
# the number of minutes in a non-leap year (1900 and 2005)
#
# doctest: I pass in 2012 and it returns true
# >> leap_year? 2012
# => true
#
# doctest: I pass in 2013 and it returns false
# >> leap_year? 2013
# => false
#
# doctest: I pass in 2000 and it returns true
# >> leap_year? 2000
# => true
#
# doctest: I pass in 1900 and it returns false
# >> leap_year? 1900
# => false
#
# doctest: I get minutes in leap year
# >> minutes_in_year 2000
# => 527040
#
# doctest: I get minutes in non-leap year
# >> minutes_in_year 1900
# => 525600

def leap_year? year
  year % 4 == 0 && year % 100 != 0 || year % 400 == 0
end

def minutes_in_year year
  60 * 24 * (leap_year?(year) ? 366 : 365)
end

[2000, 2004, 1900, 2005].each do |year|
  puts "There are #{minutes_in_year year} mins in year #{year}."
end
