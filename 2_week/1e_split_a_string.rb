# Write a program that processes the string 
# s = "Welcome to the forum.\nHere you can learn Ruby.\nAlong with other members.\n"
# a line at a time, using all that we have learned so far. The expected output is:
#
# >ruby tmp.rb
#
# Line 1: Welcome to the forum.
#
# Line 2: Here you can learn Ruby.
#
# Line 3: Along with other members.

# doctest: I enter a string to get separated output
# >> split_string "Welcome to the forum.\nHere you can learn Ruby"
# => Line 1: Welcome to the forum.

def split_string str
  str.split("\n").each_with_index {|line, index|
    puts "Line #{index + 1}: #{line}"
  }
end

if __FILE__ == $PROGRAM_NAME
  split_string "Welcome to the forum.\nHere you can learn Ruby.\nAlong with other members.\n"
end
